#ifndef audio_class_hpp
#define audio_class_hpp

#include "misc_output_classes.hpp"
#include <SFML/Audio/SoundBuffer.hpp>

class audio
{
private:		// variables
	const sf::Int16* internal_samples;
	sf::Uint64 internal_sample_count;
	unsigned int internal_sample_rate;
	unsigned int internal_channel_count;
	
	
public:		// functions
	inline audio( const sf::SoundBuffer& exit_jazz_band )
	{
		*this = exit_jazz_band;
	}
	inline audio& operator = ( const sf::SoundBuffer& exit_jazz_band )
	{
		internal_samples = exit_jazz_band.getSamples();
		internal_sample_count = exit_jazz_band.getSampleCount();
		internal_sample_rate = exit_jazz_band.getSampleRate();
		internal_channel_count = exit_jazz_band.getChannelCount();
		
		return *this;
	}
	
	
	inline void print_header() const
	{
		printout( internal_sample_count, ", ", internal_sample_rate, ", ", 
			internal_channel_count, "\n\n" );
	}
	
	void print_samples() const;
	
	inline void print() const
	{
		print_header();
		print_samples();
	}
	
	inline const sf::Int16* samples() const
	{
		return internal_samples;
	}
	inline sf::Uint64 sample_count() const
	{
		return internal_sample_count;
	}
	
	inline unsigned int sample_rate() const
	{
		return internal_sample_rate;
	}
	inline unsigned int channel_count() const
	{
		return internal_channel_count;
	}
};

#endif		// audio_class_hpp
