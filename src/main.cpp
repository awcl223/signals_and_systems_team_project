#include "audio_class.hpp"
#include "Yin.h"
#include <iomanip>
#include <sstream>
#include <memory>
#include <fstream>



std::string get_quoted_str( const std::string& str )
{
	//std::stringstream sstm;
	//
	//sstm << std::quoted(str);
	//return sstm.str();
	std::string ret = std::string("\"") + str + std::string("\"");
	
	return ret;
}

float find_pitch( audio& the_audio, int* divisor=nullptr, 
	float* probability=nullptr )
{
	Yin yang;
	
	std::unique_ptr<float[]> buf;
	buf.reset(new float[the_audio.sample_count()]);
	
	for ( size_t i=0; i<the_audio.sample_count(); ++i )
	{
		buf[i] = static_cast<float>(the_audio.samples()[i]);
	}
	
	int temp_divisor;
	float pitch = 0.0f;
	
	
	//for ( temp_divisor=10; temp_divisor>0; --temp_divisor )
	for ( temp_divisor=8; temp_divisor>0; --temp_divisor )
	{
		//printout( "Current temp_divisor:  ", temp_divisor, "\n" );
		
		yang.initialize( the_audio.sample_rate(), 
			the_audio.sample_count() / temp_divisor, 0.10f );
		
		pitch = yang.getPitch(buf.get());
		yang.cleanUp();
		
		if ( pitch >= 10 )
		{
			break;
		}
	}
	
	if (divisor)
	{
		*divisor = temp_divisor;
	}
	
	if (probability)
	{
		*probability = yang.getProbability();
	}
	
	return pitch;
}

inline void find_and_print_pitch( audio& the_audio )
{
	//printout( "Pitch is found to be ", pitch, " with buffer_length ",
	//	buffer_length, " and probability ", yang.getProbability(),
	//	"\n" );
	
	//printout( "Pitch is found to be ", pitch, " with divisor ", divisor,
	//	" and probability ", yang.getProbability(), "\n" );
	
	
	//printout(pitch);
	//float pitch = find_pitch(the_audio);
	
	std::ofstream f( "pitch.txt", std::ios_base::out 
		| std::ios_base::trunc );
	f << find_pitch(the_audio);
}

int finish_any_run( const std::string& fname )
{
	// Used to actually extract audio from the input audio file
	sf::SoundBuffer exit_jazz_band;
	exit_jazz_band.loadFromFile(fname);
	
	audio the_audio(exit_jazz_band);
	find_and_print_pitch(the_audio);
	
	return 0;
}

int run_with_ffmpeg( const std::string& input_fname, 
	const std::string& temp_fname )
{
	// Run ffmpeg to convert the audio file to .wav because, due to
	// patents, SFML doesn't support MP3s.  I had a hard time figuring
	// out how I was actually going to extract the actual audio from
	// the MP3 file.
	
	// The "ffmpeg" options used are as follows
	// 
	// "-i \"input_fname\"" is used to input a file to the "ffmpeg"
	// command
	// 
	// "-ac 1 " is used to convert the input source file
	{
		const std::string ffmpeg_cmd 
			= std::string("ffmpeg -i ")
			+ get_quoted_str(input_fname)
			+ std::string(" -ac 1 ")
			+ std::string(" ")
			+ get_quoted_str(temp_fname);
		
		printout( ffmpeg_cmd, "\n" );
		
		system(ffmpeg_cmd.c_str());
	}
	
	return finish_any_run(temp_fname);
}

int main( int argc, char** argv )
{
	// Fun with the preprocessor
	#ifdef require_ffmpeg
	static constexpr int allowed_argc = 3;
	
	if ( argc != allowed_argc )
	{
		printerr( "Usage:  ", argv[0], " mp3_input_file temp_wav_file\n" );
		return 1;
	}
	const std::string input_fname = argv[1];
	const std::string temp_fname = argv[2];
	
	return run_with_ffmpeg( input_fname, temp_fname );
	#else
	static constexpr int allowed_argc = 2;
	if ( argc != allowed_argc )
	{
		printerr( "Usage:  ", argv[0], " wav_input_file\n" );
		return 1;
	}
	
	const std::string input_fname = argv[1];
	
	return finish_any_run(input_fname);
	#endif
}
