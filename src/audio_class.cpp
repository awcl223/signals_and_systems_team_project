#include "audio_class.hpp"


void audio::print_samples() const
{
	for ( uint64_t i=0; i<sample_count(); ++i )
	{
		printout( samples()[i], "\n" );
	}
}
