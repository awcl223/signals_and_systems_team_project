# These directories specify where source code files are located.
# Edit these variables if more directories are needed.  
# Separate each entry by spaces.
CXX_DIRS:=$(CURDIR) src

# End of source directories


OPTIMIZATION_LEVEL:=-O2

# This is the name of the output file.  Change this if needed!
PROJ:=$(shell basename $(CURDIR))


#CROSS:=i686-w64-mingw32.static-
ifdef CROSS
	PROJ:=$(PROJ).exe
endif

# Compilers and initial compiler flags
CXX:=$(CROSS)g++
CXX_FLAGS:=$(CXX_FLAGS) -std=c++11 -Wall $(shell $(CROSS)pkg-config \
	--cflags sfml-all)

# The linker (just g++)
LD:=$(CXX)

# Initial linker flags
LD_FLAGS:=$(LD_FLAGS) -lm $(shell $(CROSS)pkg-config --libs sfml-all)



FINAL_BASE_FLAGS:=$(OPTIMIZATION_LEVEL) $(EXTRA_BASE_FLAGS)

# Final compiler and linker flags
CXX_FLAGS:=$(CXX_FLAGS) $(FINAL_BASE_FLAGS)
LD_FLAGS:=$(LD_FLAGS) $(EXTRA_LD_FLAGS)




# Generated directories
OBJDIR:=objs
DEPDIR:=deps
PREPROCDIR:=preprocs

# Directories to search, specified at the top of this file
export VPATH	:=	\
	$(foreach DIR,$(CXX_DIRS),$(CURDIR)/$(DIR)) \


CXX_SOURCES:=$(foreach DIR,$(CXX_DIRS),$(notdir $(wildcard $(DIR)/*.cpp)))
CXX_OFILES:=$(CXX_SOURCES:%.cpp=$(OBJDIR)/%.o) 
CXX_PFILES:=$(CXX_SOURCES:%.cpp=$(DEPDIR)/%.P) 
 


# Compiler-generated files
# OFILES are object code files (extension .o)
OFILES:=$(CXX_OFILES) 
# PFILES are used for automatic dependency generation
PFILES:=$(CXX_PFILES) 

all : all_pre $(OFILES)
	$(LD) $(OBJDIR)/*.o -o $(PROJ) $(LD_FLAGS)

all_pre :
	mkdir -p $(OBJDIR) $(DEPDIR)


# This sed script is basically a hack.  It is used to convert dependency
# files.
sed_script:=$(shell echo "sed -e 's/\#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' -e '/^$$/ d' -e 's/$$/ :/'")


# Here's where things get really messy.
$(CXX_OFILES) : $(OBJDIR)/%.o : %.cpp 
	@echo $@" was updated or has no object file.  (Re)Compiling...." 
	$(CXX) $(CXX_FLAGS) -MMD -c $< -o $@ 
	@cp $(OBJDIR)/$*.d $(DEPDIR)/$*.P 
	@$(sed_script) < $(OBJDIR)/$*.d >> $(DEPDIR)/$*.P 
	@rm -f $(OBJDIR)/$*.d


-include $(PFILES)


#¯\(°_o)/¯

.PHONY : clean
clean :
	rm -rfv $(OBJDIR) $(DEPDIR) $(ASMOUTDIR) $(PREPROCDIR) $(PROJ) tags *.taghl gmon.out

